﻿using celincapi.Models;
using celincapi.Utils;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace celincapi.Controllers
{
    public class JobsController : ApiController
    {
        public IEnumerable<Job> Get()
        {
            return MongoBridge.FindAll<Job>().Where(j => j.IsAtive);
        }

        public Job Get(string id)
        {
            var talent = MongoBridge.Find<Job>(id);
            return talent;
        }

        public void Post([FromBody]Job job)
        {
            job.Create();
        }

        public void Put(string id, [FromBody]Job data)
        {
            var job = MongoBridge.Find<Job>(id);

            // this look a bit tedious, it can be improved by using automapper. For this simple app, i am not using it. 
            job.FirstName = data.FirstName;
            job.LastName = data.LastName;
            job.ContactNumber = data.ContactNumber;
            job.Company = data.Company;
            job.Email = data.Email;
            job.Title = data.Title;
            job.Description = data.Description;

            job.Update();
        }

        public void Delete(string id)
        {
            var talent = MongoBridge.Find<Job>(id);
            talent.IsAtive = false;
            talent.Update();
        }
    }
}
