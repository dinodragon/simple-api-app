﻿using celincapi.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Security.Authentication;

namespace celincapi.Utils
{
    public class MongoBridge
    {
        private const string _connectionString = @"mongodb://celincapis:bB8a3OpzpElbVKmaWgg3u3gZTZHdu5hPPE8OUGLnFvrRZdNbvMFN4ca4Vz2wPwUa9huoOPs05ONqvCrXsAIyCA==@celincapis.documents.azure.com:10255/?ssl=true&replicaSet=globaldb";
        private static MongoClient _client { get; set; }

        private static MongoBridge _instance { get; set; }

        private static IMongoDatabase _db;

        private MongoBridge()
        {
            MongoClientSettings settings = MongoClientSettings.FromUrl(new MongoUrl(_connectionString));
            settings.SslSettings = new SslSettings() { EnabledSslProtocols = SslProtocols.Tls12 };
            _client = new MongoClient(settings);
            _db = _client.GetDatabase("test");
        }

        public static MongoBridge getInstance()
        {
            if (_instance == null)
                _instance = new MongoBridge();

            return _instance;
        }

        public static T Find<T>(string id) where T : MongoDocument
        {
            getInstance();

            var filter = Builders<T>.Filter.Eq("_id", id);
            return _db.GetCollection<T>(typeof(T).Name).Find(filter).FirstOrDefault();
        }

        public static IEnumerable<T> FindAll<T>() where T : MongoDocument
        {
            getInstance();

            var filter = Builders<T>.Filter.Empty;
            return _db.GetCollection<T>(typeof(T).Name).Find(filter).ToEnumerable();
        }

        public bool Create<T>(T data) where T : MongoDocument
        {
            try
            {
                _db.GetCollection<BsonDocument>(data.GetType().Name).InsertOne(data.ToBsonDocument());
            }
            catch(Exception)
            {
                return false;
            }

            return true;
        }

        public bool Update<T>(T data) where T : MongoDocument
        {
            try
            {
                _db.GetCollection<T>(data.GetType().Name).ReplaceOne(d => d.Id == data.Id, data);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }
    }
}