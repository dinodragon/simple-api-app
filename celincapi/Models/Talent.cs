﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace celincapi.Models
{
    public class Job : MongoDocument
    {
        [BsonElement("firstName")]
        public string FirstName { get; set; }

        [BsonElement("lastName")]
        public string LastName { get; set; }

        [BsonElement("contactNumber")]
        public string ContactNumber { get; set; }

        [BsonElement("company")]
        public string Company { get; set; }

        [BsonElement("email")]
        public string Email { get; set; }

        [BsonElement("title")]
        public string Title { get; set; }

        [BsonElement("description")]
        public string Description { get; set; }

        [BsonDefaultValue(true)]
        public bool IsAtive { get; set; }
    }
}