﻿using celincapi.Utils;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;


namespace celincapi.Models
{
    public class MongoDocument
    {
        private MongoBridge _client;

        [BsonElement("id")]
        public string Id { get; set; }
        
        public MongoDocument()
        {
            _client = MongoBridge.getInstance();
        }

        public void Create()
        {
            Id = Guid.NewGuid().ToString();
            _client.Create(this);
        }

        public bool Update()
        {
            if (Id == null)
                return false;

            return _client.Update(this);
        }
    }
}